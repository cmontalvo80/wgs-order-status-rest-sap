package com.kubota.kl.wgs.rest.sap;

/**
 * @author Chris Montalvo
 *
 */

public class OrderStatusOptions {
	
	private final long id;
	private String dealerNumber;
	
	private String[] productTypeList;
	private String[] dbsList;
	private String[] orderStatusList;

	public OrderStatusOptions(long id, String dealerNumber, String[] productTypeList, String[] dbsList, String[] orderStatusList) {
		this.id = id;
		this.dealerNumber = dealerNumber;
		this.productTypeList = productTypeList;
		this.dbsList = dbsList;
		this.orderStatusList = orderStatusList;
	}

	public String getDealerNumber() {
		return dealerNumber;
	}

	public void setDealerNumber(String dealerNumber) {
		this.dealerNumber = dealerNumber;
	}

	public String[] getProductTypeList() {
		return productTypeList;
	}

	public void setProductTypeList(String[] productTypeList) {
		this.productTypeList = productTypeList;
	}

	public String[] getDbsList() {
		return dbsList;
	}

	public void setDbsList(String[] dbsList) {
		this.dbsList = dbsList;
	}

	public String[] getOrderStatusList() {
		return orderStatusList;
	}

	public void setOrderStatusList(String[] orderStatusList) {
		this.orderStatusList = orderStatusList;
	}
	
}
