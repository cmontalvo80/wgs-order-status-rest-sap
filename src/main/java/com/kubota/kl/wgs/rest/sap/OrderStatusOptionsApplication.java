package com.kubota.kl.wgs.rest.sap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderStatusOptionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderStatusOptionsApplication.class, args);
	}
}
