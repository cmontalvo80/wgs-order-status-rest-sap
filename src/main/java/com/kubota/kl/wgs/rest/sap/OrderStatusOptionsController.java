package com.kubota.kl.wgs.rest.sap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderStatusOptionsController {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/wgsInquiryOptions")
    public OrderStatusOptions getInquiryOptions(@RequestParam(value="dealerNumber", defaultValue="") String dealerNumber) {
    	
    	String[] productTypeList = new String[5];
    	productTypeList[0] = "TRACTORS";
    	productTypeList[1] = "TIRES";
    	productTypeList[2] = "SPREADER";
    	productTypeList[3] = "ROPS";
    	productTypeList[4] = "REPLACEMENT ROPS";
    	
    	String[] dbsList = new String[4];
    	dbsList[0] = "AP";
    	dbsList[1] = "B";
    	dbsList[2] = "BX";
    	dbsList[3] = "BXTLB";
    	/*dbsList.add("DM");
    	dbsList.add("F");
    	dbsList.add("G");
    	dbsList.add("GR");
    	dbsList.add("K/R");
    	dbsList.add("L");
    	dbsList.add("L-A");
    	dbsList.add("M-F");
    	dbsList.add("M-S");
    	dbsList.add("M-T");
    	dbsList.add("RA");
    	dbsList.add("RB");
    	dbsList.add("S");
    	dbsList.add("SL");
    	dbsList.add("T");
    	dbsList.add("TE");
    	dbsList.add("V");
    	dbsList.add("VS");
    	dbsList.add("W-S");
    	dbsList.add("Z");*/
    	
    	String[] orderStatusList = new String[4];
    	orderStatusList[0] = "ASSEMBLY ARRANGED";
    	orderStatusList[1] = "ASSEMBLY COMPLETED";
    	orderStatusList[2] = "ASSEMBLY ON HOLD";
    	orderStatusList[3] = "ASSEMBLY SCHED FINAL";
    	
    	OrderStatusOptions options = new OrderStatusOptions(counter.incrementAndGet(), dealerNumber, productTypeList, dbsList, orderStatusList);
    	
    	return options;
    }
}
