# WGS Order Status SAP REST Services

This project holds all of the REST services for Wholegoods Order Status that retrieve data from SAP.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
JDK 1.8 or later
Gradle 4+
Spring Tool Suite (STS)
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Once you have downloaded this project from REPO:

Within your IDE import a gradle project:
1. File -> Import
2. At import wizard (popup) select "Gradle" and click on "Existing Gradle Project"
3. Click Next
4. Click Next
5. At the next Import Gradle Project screen, specify the local directory where your downloaded project is located.
6. Once directory has been specificed, click on Finish.

```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment



## Built With



## Contributing


## Versioning


## Authors

* ** Chris Montalvo ** 


## License


## Acknowledgments




